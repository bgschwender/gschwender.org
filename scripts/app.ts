/// <reference path="references.d.ts" />
/// <reference path="custom.d.ts" />

module bg {
  export function init() {
    var me = this;

    // Responsive CSS
    me.onWindow(function () {
      if (me.isMobileView()) {
        $("body").addClass("mobile");
      } else {
        $("body").removeClass("mobile");
      }
      if (me.isTabletView()) {
        $("body").addClass("tablet");
      } else {
        $("body").removeClass("tablet");
      }
    });

    // Top-Header
    var header = $(".header-top");
    onWindow(function(){
      var win = $(window);
      if (win.scrollTop() >= 500) {
        header.addClass("dark");
      } else {
        header.removeClass("dark");
      }
    });
  }

  export function onResize(callback: Function) {
    var me = this;
    var resizeTimer;
    $(window).resize(function () {
      window.clearTimeout(resizeTimer);
      resizeTimer = window.setTimeout(function () {
        executeCallback(callback);
      }, 10);
    });
  }

  export function onScroll(callback: Function) {
    var scrollTimer;
    $(document).on("scroll", function () {
      window.clearTimeout(scrollTimer);
      scrollTimer = window.setTimeout(function () {
        executeCallback(callback);
      }, 10);
    });
  }

  export function onWindow(callback: Function) {
    onResize(callback);
    onScroll(callback);
    executeCallback(callback);
  }

  export function isMobileView(): boolean {
    return $(window).width() <= 768;
  }

  export function isTabletView(): boolean {
    return $(window).width() <= 992 && !this.isMobileView();
  }

  export function isPrintView(): boolean {
    if (window["matchMedia"]) {
      var m = window.matchMedia("print");
      if (m) {
        return m.matches;
      } else {
        return false;
      }
    } else {
      // Fallback
      return this._isPrintView;
    }
  }

  export function ready(callback: Function) {

  }

  export function wait(check: { (): boolean }, callback: Function) {
    var me = this;
    window.setTimeout(function () {
      if (check()) {
        callback();
      } else {
        me.wait(check, callback);
      }
    }, 100);
  }

  export function executeCallback(callback: any, data: any = undefined, thisArg: any = undefined): void {
    if (callback !== undefined && callback !== null) {
      if (data && !$.isArray(data))
        data = [data];
      if (!$.isArray(callback)) {
        callback.apply(thisArg, data);
      } else {
        var callbackArray: { (data: any): void; }[] = callback;
        for (var i: number = 0, length: number = callbackArray.length; i < length; i++) {
          var cb = callbackArray[i];
          if (cb)
            cb.apply(thisArg, data);
        }
      }
    }
  }

  export function scrollTo(name: string) {
    var target = $("[id=" + name + "]");
    if (target && target.length > 0) {
      scrollToElement(target);
      return true;
    }
    return false;
  }

  export function scrollToElement(target: JQuery) {
    $('html, body').animate({
      scrollTop: (target.offset().top - (isMobileView() ? 60 : 100))
    }, 1000);
  }

  export function toggleMenue() {
    $("body").toggleClass("open-menue");
  }

  export function showImprint() {
    var el = $("#imprint");;
    el.removeClass("hidden");
    scrollToElement(el);
  }
}

var ssc = 0;
function startApp() {
  window.setTimeout(function(){
    if(window["jQuery"]) {
      if(!window["$"]) {
        window["$"] = jQuery;
      }
      ssc = 1;
      $(document).ready(function () {
        bg.init();
        $.each((<any>window).readyCallbacks, function(i,cb) {
          cb();
        });
      });
    } else {
      startApp();
    }
  }, 50);
}
startApp();