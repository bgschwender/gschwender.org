/// <reference path="references.d.ts" />
/// <reference path="custom.d.ts" />
var bg;
(function (bg) {
    function init() {
        var me = this;
        // Responsive CSS
        me.onWindow(function () {
            if (me.isMobileView()) {
                $("body").addClass("mobile");
            }
            else {
                $("body").removeClass("mobile");
            }
            if (me.isTabletView()) {
                $("body").addClass("tablet");
            }
            else {
                $("body").removeClass("tablet");
            }
        });
        // Top-Header
        var header = $(".header-top");
        onWindow(function () {
            var win = $(window);
            if (win.scrollTop() >= 500) {
                header.addClass("dark");
            }
            else {
                header.removeClass("dark");
            }
        });
    }
    bg.init = init;
    function onResize(callback) {
        var me = this;
        var resizeTimer;
        $(window).resize(function () {
            window.clearTimeout(resizeTimer);
            resizeTimer = window.setTimeout(function () {
                executeCallback(callback);
            }, 10);
        });
    }
    bg.onResize = onResize;
    function onScroll(callback) {
        var scrollTimer;
        $(document).on("scroll", function () {
            window.clearTimeout(scrollTimer);
            scrollTimer = window.setTimeout(function () {
                executeCallback(callback);
            }, 10);
        });
    }
    bg.onScroll = onScroll;
    function onWindow(callback) {
        onResize(callback);
        onScroll(callback);
        executeCallback(callback);
    }
    bg.onWindow = onWindow;
    function isMobileView() {
        return $(window).width() <= 768;
    }
    bg.isMobileView = isMobileView;
    function isTabletView() {
        return $(window).width() <= 992 && !this.isMobileView();
    }
    bg.isTabletView = isTabletView;
    function isPrintView() {
        if (window["matchMedia"]) {
            var m = window.matchMedia("print");
            if (m) {
                return m.matches;
            }
            else {
                return false;
            }
        }
        else {
            // Fallback
            return this._isPrintView;
        }
    }
    bg.isPrintView = isPrintView;
    function ready(callback) {
    }
    bg.ready = ready;
    function wait(check, callback) {
        var me = this;
        window.setTimeout(function () {
            if (check()) {
                callback();
            }
            else {
                me.wait(check, callback);
            }
        }, 100);
    }
    bg.wait = wait;
    function executeCallback(callback, data, thisArg) {
        if (data === void 0) { data = undefined; }
        if (thisArg === void 0) { thisArg = undefined; }
        if (callback !== undefined && callback !== null) {
            if (data && !$.isArray(data))
                data = [data];
            if (!$.isArray(callback)) {
                callback.apply(thisArg, data);
            }
            else {
                var callbackArray = callback;
                for (var i = 0, length = callbackArray.length; i < length; i++) {
                    var cb = callbackArray[i];
                    if (cb)
                        cb.apply(thisArg, data);
                }
            }
        }
    }
    bg.executeCallback = executeCallback;
    function scrollTo(name) {
        var target = $("[id=" + name + "]");
        if (target && target.length > 0) {
            scrollToElement(target);
            return true;
        }
        return false;
    }
    bg.scrollTo = scrollTo;
    function scrollToElement(target) {
        $('html, body').animate({
            scrollTop: (target.offset().top - (isMobileView() ? 60 : 100))
        }, 1000);
    }
    bg.scrollToElement = scrollToElement;
    function toggleMenue() {
        $("body").toggleClass("open-menue");
    }
    bg.toggleMenue = toggleMenue;
    function showImprint() {
        var el = $("#imprint");
        ;
        el.removeClass("hidden");
        scrollToElement(el);
    }
    bg.showImprint = showImprint;
})(bg || (bg = {}));
var ssc = 0;
function startApp() {
    window.setTimeout(function () {
        if (window["jQuery"]) {
            if (!window["$"]) {
                window["$"] = jQuery;
            }
            ssc = 1;
            $(document).ready(function () {
                bg.init();
                $.each(window.readyCallbacks, function (i, cb) {
                    cb();
                });
            });
        }
        else {
            startApp();
        }
    }, 50);
}
startApp();
//# sourceMappingURL=app.js.map