<?php
/*
 * Main Application Class
 *
 * (c) Benjamin Gschwender <b@gschwender.org>
 */

namespace Gitware\SimplePage;

use Gitware\SimplePage\Config;

class App {
  const VERSION = '1.0.0';
  const VERSION_ID = 100000;

  private static $_cache = array();
  private static $_config;
  
  /**
  * Load the Main Gitware SimplePage Main Application
  */
  public static function load() {
    self::$_config = new Config();
  }

  public static function getConfig($instance=true) {
    if($instance) {
      return self::$_config->getInstance();
    } else {
      return self::$_config;
    }
  }

}

App::load();