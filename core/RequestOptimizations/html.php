<?php
require_once __DIR__ . '/lib/html.php';
function gitware_minifyhtml($buffer)
{
  $html = Minify_HTML::minify($buffer);
  header('Content-Encoding: deflate' );
  return gzdeflate($html);
}
ob_start("gitware_minifyhtml");
?>