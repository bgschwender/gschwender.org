<?php
require_once __DIR__ . '/functions.php';

$config = Gitware\SimplePage\App::getConfig();
if($config->minify_html) {
  require_once __DIR__ . '/html.php';
}