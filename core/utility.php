<?php

function handleDebug($force=false) {
  if(tgoRequestParam("debug") == "1" || $force) {
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ERROR | E_WARNING | E_PARSE);
    return true;
  }
  return false;
}

function setLastModifiedHeader($lastModifiedTimestamp, $dauer=60) {
  $exp_gmt = gmdate("D, d M Y H:i:s", $lastModifiedTimestamp + $dauer * 60) ." GMT";
  $mod_gmt = gmdate("D, d M Y H:i:s", $lastModifiedTimestamp) ." GMT";

  header("Expires: " . $exp_gmt);
  header("Last-Modified: " . $mod_gmt);
  header("Cache-Control: private, max-age=" . $dauer * 60);
  // Speziell für MSIE 5
  header("Cache-Control: pre-check=" . $dauer * 60, FALSE);
  // Old
  header("Pragma: cache");
}

function getClientIp() {
  $clientip = "unknown";
  if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
    $clientip = $_SERVER['HTTP_CLIENT_IP'];
  } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
    $clientip = $_SERVER['HTTP_X_FORWARDED_FOR'];
  } else {
    $clientip = $_SERVER['REMOTE_ADDR'];
  }
  return $clientip;
}

function setQueryParameter($url, $parameter, $value, $overwrite = true) {
  if($value && $value !="") {
    $url1 = $url;
    $hash = "";
    $h = strpos($url, "#");
    if($h > 0) {
      $hash = substr($url, $h);
      $url1 = substr($url, 0, $h);
    }
    $query = array();
    $p = strpos($url1, "?");
    if (is_numeric($p) && $p >= 0) {
      parse_str(substr($url1, ($p + 1)), $query);
      $url1 = substr($url1, 0, $p);
    }

    if ($overwrite) {
      $query[$parameter] = $value;
    } else {
      $frags = explode(",", $query[$parameter]);
      $frags[] = $value;
      $query[$parameter] = $frags; // implode(",", $frags);
    }

    $result = "";
    if (count($query) > 0) {
      $result = $url1 . "?" . http_build_query($query);
    } else {
      $result = $url1;
    }
    if(!empty($hash)) {
      $result .= $hash;
    }
    return $result;
  } else {
    return $url;
  }
}

function removeQueryParameter($url, $parameter) {
  $query = array();
  $p = strpos($url, "?");
  $url1 = "";
  if(is_numeric($p) && $p>=0) {
    $url1 = substr($url, 0, $p);
    parse_str(substr($url, ($p + 1)), $query);
  } else {
    $url1 = $url;
  }
  unset($query[$parameter]);
  if(count($query)>0) {
    return $url1 . "?" . http_build_query($query);
  } else {
    return $url1;
  }
}

function requestParam($name) {
  $value = getValueByKey($_POST, $name);
  if(!$value || $value == "") {
    $value = getValueByKey($_GET, $name);
  }
  return $value;
}

function getValueByKey($array, $key) {
  foreach($array as $k=>$v) {
    if($k===$key) {
      return $v;
    }
  }
  return null;
}

function allRequestParameters() {
  $o = array();
  foreach($_GET as $k=>$v) {
    $o[$k] = $v;
  }
  foreach($_POST as $k=>$v) {
    $o[$k] = $v;
  }
  return $o;
}

function permanentRedirect($url) {
  header("HTTP/1.1 301 Moved Permanently");
  header("Location: $url");
}


function redirect($url) {
  header("Location: $url");
}

function openJson($filename) {
  $str = file_get_contents_utf8($filename);
  $enc = mb_detect_encoding($str);
  if($enc != "UTF-8") {
    $str = utf8_encode($str);
  }
  $json = json_decode2($str);
  return $json;
}

function saveJson($obj, $filename) {
  $json = json_encode(utf8_encode2($obj));
  $dir = dirname($filename);
  if(!is_dir($dir))
    mkdir($dir);
  file_put_contents($filename, $json);
}

function api_request($uri, $method="GET", $fields=NULL, $timeout = 5, $debug = false) {
  $fields_string = "";
  if($fields && is_array($fields)) {
    foreach ($fields as $key => $value) {
      $fields_string .= $key . '=' . urlencode($value) . '&';
    }
  }
  rtrim($fields_string, '&');

  $handle = curl_init();
  $options = array(
    CURLOPT_HEADER => false,
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_TIMEOUT => $timeout,
    CURLOPT_USERAGENT => "Trendgo-Shop",
    CURLOPT_URL => $uri,
    CURLOPT_CUSTOMREQUEST => $method, // GET POST PUT PATCH DELETE HEAD OPTIONS
  );

  switch($method) {
    default:
    case "POST":
      $options[CURLOPT_POSTFIELDS] = $fields_string;
      break;

    case "GET":
      $options[CURLOPT_URL] = $uri . "?" . $fields_string;
      break;

    case "JSON-POST":
      $data_string = json_encode(utf8_encode2($fields));
      $options[CURLOPT_CUSTOMREQUEST] = "POST";
      $options[CURLOPT_POSTFIELDS] = $data_string;
      $options[CURLOPT_HTTPHEADER] = array(
        'Content-Type: application/json; charset=utf-8;',
        'Content-Length: ' . strlen($data_string)
      );
      break;
  }

  if($debug) {
    echo "<pre>" . print_r($options, 1) . "</pre>";
  }

  curl_setopt_array($handle, $options);
  $resp = curl_exec($handle);
  $info = curl_getinfo($handle);
  curl_close($handle);
  if($debug) {
    echo "<pre>" . print_r($resp, 1) . "</pre>";
    echo "<pre>" . print_r($info, 1) . "</pre>";
  }
  if(intval($info["http_code"]) != 200) {
    // TODO: 
    $resp = null;
  }
  return $resp;
}

function json_request($uri, $method, $fields=NULL, $timeout = 5, $decode = true) {
  $resp =  api_request($uri, $method, $fields, $timeout);
  $result = json_decode($resp);
  if($decode) {
    $result = utf8_decode2($result);
  }
  return $result;
}

function json_decode2($str) {
  if($str && !empty($str) && gettype($str) == "string") {
    $json = utf8_encode2($str);
    if (startsWith($json, "\xEF\xBB\xBF"))
      $json = substr($str, 3);
    return json_decode($json);
  }
  return null;
}

function json_encode2($obj) {
  $json = json_encode($obj, JSON_UNESCAPED_UNICODE);
  if(!$json || $json == "") {
    $utf8Obj = utf8_encode2($obj);
    $json = json_encode($utf8Obj, JSON_UNESCAPED_UNICODE);
  }
  return decodeUnicode($json);
}

function utf8_decode2($obj, $html=false) {
  $res = $obj;
  if(is_array($obj)) {
    $res = array();
    $aKeys = array_keys($obj);
    $aVals = array_values($obj);
    for ($x=0;$x<count($aKeys);$x++) {
      $res[$aKeys[$x]] = utf8_decode2($aVals[$x], $html);
    }
  }
  else if(is_object($obj)) {
    $res = clone $obj;
    foreach($res as $field=>$val) {
      $res->$field = utf8_decode2($val, $html);
    }
  } else if(is_string($obj)) {
    if($html) {
      $res = htmlentities($res, ENT_NOQUOTES);
    }
    $enc = mb_detect_encoding($res, 'UTF-8, ISO-8859-1');
    if($enc === "UTF-8") {
      $res = utf8_decode($res);
    }
  }
  return $res;
}

function utf8_encode2($obj, $html=false) {
  $res = $obj;
  if(is_array($obj)) {
    $res = array();
    $aKeys = array_keys($obj);
    $aVals = array_values($obj);
    for ($x=0;$x<count($aKeys);$x++) {
      $res[$aKeys[$x]] = utf8_encode2($aVals[$x], $html);
    }
  }
  else if(is_object($obj)) {
    $res = clone $obj;
    foreach($res as $field=>$val) {
      $res->$field = utf8_encode2($val, $html);
    }
  } else if(is_string($obj)) {
    if($html) {
      $res = html_entity_decode($res);
    }
    $enc = mb_detect_encoding($res, 'UTF-8, ISO-8859-1');
    if($enc !== "UTF-8") {
      $res = utf8_encode($res);
    }
  }
  return $res;
}

if (!function_exists('strcontains')) {
  function strcontains($haystack,$needle) {
    if  (strpos($haystack,$needle)!==false)
      return true;
    else
      return false;

  }
}

if (!function_exists('startsWith')) {
  function startsWith($haystack, $needle) {
    return !strncmp($haystack, $needle, strlen($needle));
  }
}

if (!function_exists('endsWith')) {
  function endsWith($haystack, $needle) {
    $length = strlen($needle);
    if ($length == 0) {
      return true;
    }
    return (substr($haystack, -$length) === $needle);
  }
}

if (!function_exists('file_get_contents_utf8')) {
  function file_get_contents_utf8($fn) {
    $opts = array(
      'http' => array(
        'method'=>"GET",
        'header'=>"Content-Type: text/html; charset=utf-8"
      )
    );
    $context = stream_context_create($opts);
    $result = @file_get_contents($fn,false,$context);
    return $result;
  }
}

if (!function_exists('json_indent')) {
  function json_indent($json) {

    $result = '';
    $pos = 0;
    $strLen = strlen($json);
    $indentStr = '  ';
    $newLine = "\n";
    $prevChar = '';
    $outOfQuotes = true;

    for ($i=0; $i<=$strLen; $i++) {

      // Grab the next character in the string.
      $char = substr($json, $i, 1);

      // Are we inside a quoted string?
      if ($char == '"' && $prevChar != '\\') {
        $outOfQuotes = !$outOfQuotes;

        // If this character is the end of an element,
        // output a new line and indent the next line.
      } else if(($char == '}' || $char == ']') && $outOfQuotes) {
        $result .= $newLine;
        $pos --;
        for ($j=0; $j<$pos; $j++) {
          $result .= $indentStr;
        }
      }

      // Add the character to the result string.
      $result .= $char;

      // If the last character was the beginning of an element,
      // output a new line and indent the next line.
      if (($char == ',' || $char == '{' || $char == '[') && $outOfQuotes) {
        $result .= $newLine;
        if ($char == '{' || $char == '[') {
          $pos ++;
        }

        for ($j = 0; $j < $pos; $j++) {
          $result .= $indentStr;
        }
      }

      $prevChar = $char;
    }

    return $result;
  }
}

function array_sort($array, $on, $order=SORT_ASC) {
  $sortable_array = array();
  if (count($array) > 0) {

    foreach ($array as $k => $v) {
      if (is_array($v) || is_object($v)) {
        foreach ($v as $k2 => $v2) {
          if ($k2 == $on) {
            $sortable_array[] = $v2;
          }
        }
      } else {
        $sortable_array[$v] = $v;
      }
    }

    array_multisort($sortable_array, $array);
  }

  return $array;
}

if (!function_exists('http_send_status')) {
  function http_send_status($code = NULL) {

    if ($code !== NULL) {
      $text = '';
      switch ($code) {
        case 100: $text = 'Continue'; break;
        case 101: $text = 'Switching Protocols'; break;
        case 200: $text = 'OK'; break;
        case 201: $text = 'Created'; break;
        case 202: $text = 'Accepted'; break;
        case 203: $text = 'Non-Authoritative Information'; break;
        case 204: $text = 'No Content'; break;
        case 205: $text = 'Reset Content'; break;
        case 206: $text = 'Partial Content'; break;
        case 300: $text = 'Multiple Choices'; break;
        case 301: $text = 'Moved Permanently'; break;
        case 302: $text = 'Moved Temporarily'; break;
        case 303: $text = 'See Other'; break;
        case 304: $text = 'Not Modified'; break;
        case 305: $text = 'Use Proxy'; break;
        case 400: $text = 'Bad Request'; break;
        case 401: $text = 'Unauthorized'; break;
        case 402: $text = 'Payment Required'; break;
        case 403: $text = 'Forbidden'; break;
        case 404: $text = 'Not Found'; break;
        case 405: $text = 'Method Not Allowed'; break;
        case 406: $text = 'Not Acceptable'; break;
        case 407: $text = 'Proxy Authentication Required'; break;
        case 408: $text = 'Request Time-out'; break;
        case 409: $text = 'Conflict'; break;
        case 410: $text = 'Gone'; break;
        case 411: $text = 'Length Required'; break;
        case 412: $text = 'Precondition Failed'; break;
        case 413: $text = 'Request Entity Too Large'; break;
        case 414: $text = 'Request-URI Too Large'; break;
        case 415: $text = 'Unsupported Media Type'; break;
        case 500: $text = 'Internal Server Error'; break;
        case 501: $text = 'Not Implemented'; break;
        case 502: $text = 'Bad Gateway'; break;
        case 503: $text = 'Service Unavailable'; break;
        case 504: $text = 'Gateway Time-out'; break;
        case 505: $text = 'HTTP Version not supported'; break;
        default:
          exit('Unknown http status code "' . htmlentities($code) . '"');
          break;
      }
      $protocol = (isset($_SERVER['SERVER_PROTOCOL']) ? $_SERVER['SERVER_PROTOCOL'] : 'HTTP/1.0');
      header($protocol . ' ' . $code . ' ' . $text);
      $GLOBALS['http_response_code'] = $code;
    } else {
      $code = (isset($GLOBALS['http_response_code']) ? $GLOBALS['http_response_code'] : 200);
    }
    return $text;
  }
}