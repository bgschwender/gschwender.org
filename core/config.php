<?php
/*
 * Application Configuration Class
 *
 * (c) Benjamin Gschwender <b@gschwender.org>
 */

namespace Gitware\SimplePage;

class Config {
  private $_json;
  private $_instance;
  
  public function __construct() {
    $this->_json = file_get_contents_utf8(__DIR__ . '/../config.json');
    $this->_instance = \json_decode2($this->_json);
  }

  public function getInstance() {
    return $this->_instance;
  }
}