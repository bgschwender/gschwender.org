<?php require_once __DIR__ . '/../content/welcome.html'; ?>
<section id="content">
  <?php require_once __DIR__ . '/../content/section1.html'; ?>
  <?php require_once __DIR__ . '/../content/skills.html'; ?>
  <?php require_once __DIR__ . '/../content/contact.html'; ?>
  <?php require_once __DIR__ . '/../content/imprint.html'; ?>
</section>